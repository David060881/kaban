import KabanAPI from "../api/KabanAPI.js"
import DropZone from "./DropZone.js"
import Item from "./item.js"

export default class Column { 
    constructor(id, title){ 
        const topDropZone = DropZone.createDropZone()
        this.elements = {}
        this.elements.root  = Column.createRoot()
        this.elements.title = this.elements.root.querySelector(".kaban__column__title")
        this.elements.items = this.elements.root.querySelector(".kaban__column__items")
        this.elements.addItem = this.elements.root.querySelector(".kaban__add__item")

        this.elements.root.dataset.id = id;
        this.elements.title.textContent = title;
        this.elements.items.appendChild(topDropZone)

        this.elements.addItem.addEventListener("click", ()=> { 
                const newItem = KabanAPI.insertItem(id, "")
                this.renderItem(newItem)
        })

        KabanAPI.getItems(id).forEach(item => 
            {
                this.renderItem(item)
            })
    }

    static createRoot() {
        const range = document.createRange(); 

        range.selectNode(document.body)

        return range.createContextualFragment(`
        <div class="kaban__column">
            <div class="kaban__column__title"></div>
            <div class="kaban__column__items"></div>
            <button class="kaban__add__item" type="button" >+ Ajouter</button>
        </div>
    `).children[0]
    }

    renderItem(data) { 
          const item = new Item(data.id, data.content)
          this.elements.items.appendChild(item.elements.root)
    }
}