import Column from "./Column.js"

export default class Kaban { 
    constructor(root) {
        this.root = root

        Kaban.columns().forEach(column => { 
            const columnView = new Column(column.id, column.title)
            this.root.appendChild(columnView.elements.root)
        })
    }

    static columns(){ 
        return [ 
            {
                id:1,
                title : "A faire"
            },
            {
                id:2,
                title : "En cours"
            },
            {
                id:3,
                title : "Terminé"
            },
        ]
    }
}