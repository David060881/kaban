import KabanAPI from "../api/KabanAPI.js"

export default class DropZone{ 
    static createDropZone() { 

        const range = document.createRange()
        range.selectNode(document.body)

        const dropZone = range.createContextualFragment(` 
        <div class="kaban__dropzone"></div>
        
        `).children[0]

        dropZone.addEventListener("dragover", e => { 
            e.preventDefault()
            dropZone.classList.add("kaban__dropzone__active")
        })

        dropZone.addEventListener("dragleave",()  => { 
            dropZone.classList.remove("kaban__dropzone__active")
        })

        dropZone.addEventListener("drop" , e => { 
            e.preventDefault();
            dropZone.classList.remove("kaban__dropzone__active")
            const columnElement = dropZone.closest(".kaban__column")
            const columnId = Number(columnElement.dataset.id)
            const dropZoneInColumn = Array.from(columnElement.querySelectorAll(".kaban__dropzone"))
            const droppedIndex = dropZoneInColumn.indexOf(dropZone)
            
            const itemId = Number(e.dataTransfer.getData("text/plain"))
            const droppedItemElement =document.querySelector(`[data-id="${itemId}"]`)
            const insertAfter =  dropZone.parentElement.classList.contains(".kaban__item") ? dropZone.parentElement : dropZone
            if (droppedItemElement.contains(dropZone)){
                return
            }
            insertAfter.after(droppedItemElement)
            KabanAPI.updateItem(itemId, { 
                columnId,
                position: droppedIndex
            })
        })
        return dropZone
    }
}